/**
 * Get elements from DOM
 */
const adviceIdArea = document.getElementById('advice-id');
const adviceContentArea = document.getElementById('advice-content');
const generateButton = document.getElementById('generate-button');

/**
 * Global constants to for URL and number of milliseconds
 * for intervals
 */
const API_URL = 'https://api.adviceslip.com/advice';
const INTERVAL_MS = 5000;

/**
 * Save interval id, in case we want to clearInterval
 */
let intervalID = null;

/**
 * This function sends an HTTP request to API_URL
 * Parses response body as JSON
 * Then appends received data to DOM
 */
function fetchAdviceAndRender() {
  fetch(API_URL)
    .then((response) => response.json())
    .then(({ slip }) => {
      const { id, advice } = slip;
      adviceIdArea.textContent = `ADVICE #${id}`;
      adviceContentArea.textContent = `"${advice}"`;
    });
}

/**
 * Call this functions just to check
 */
fetchAdviceAndRender();

/**
 * Set up an interval to recall that function
 * each 5000 milliseconds
 */
intervalID = setInterval(fetchAdviceAndRender, INTERVAL_MS);

/**
 * When the generate button is clicked
 * clearInterval and just call fetchAdviceAndRender function
 */
generateButton.addEventListener('click', function () {
  if (intervalID) {
    clearInterval(intervalID);
  }
  fetchAdviceAndRender();
});
